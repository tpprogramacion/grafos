package gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

class Window extends Canvas {

    /**
     *
     */


    private static final long serialVersionUID = -4166964448327045634L;


	public Window(Map map) {

        JFrame frame = new JFrame("Maps");

        frame.setPreferredSize(new Dimension(Map.WIDTH, Map.HEIGHT));
        frame.setMaximumSize(new Dimension(Map.WIDTH, Map.HEIGHT));
        frame.setMinimumSize(new Dimension(Map.WIDTH, Map.HEIGHT));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);  //empieza en el medio

        frame.add(map);

/*
        try {
            frame.setIconImage(ImageIO.read(new File("img/icon.png")));
        } catch (IOException exc) {
            exc.printStackTrace();
        }
*/

        frame.setVisible(true);


        map.start();


    }


}
