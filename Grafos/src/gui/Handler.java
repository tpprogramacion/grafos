package gui;

import java.awt.Graphics;
import java.util.LinkedList;

/*
 * este es el que maneja a los objetos que estan en pantalla (celdas y numeros)
 * esto va a ser que se llame a los metodos tick de la celda y de numero, y sus corresponfdientes metodos render
 */


public class Handler {

   
    LinkedList<GraphicObject> objects = new LinkedList<>();
   

    public void render(Graphics g) {
    	for(GraphicObject object : objects){
    		object.render(g);
    	}
    }


    public void tick() {
    	for(GraphicObject object : objects){
    		object.tick();
    	}
    }

}