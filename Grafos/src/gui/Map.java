package gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;



public class Map extends Canvas implements Runnable{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Thread thread;
	private boolean running = false;
	private Handler handler;
    private static KeyInput keylistener;
    static final int WIDTH = 1024, HEIGHT = WIDTH / 12 * 9;
    
    public Map(){
    	handler = new Handler();
    	new Window(this);
    }
    
    public synchronized void start() {
        thread = new Thread(this);
        thread.start();
        running = true;
        requestFocusInWindow();
    }


    private synchronized void stop() {
        try {
            thread.join();
            running = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {       
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1) {
                tick();
                delta--;
            }
            if (running)
                render();
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
            }
        }
        stop();
    }
    private void render() {
        BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {                    //inicializa en null
            this.createBufferStrategy(3);
            return;
        }


        Graphics g = bs.getDrawGraphics();    //crea un link para los graficos y el buffer
        g.setColor(new Color(0x000000));
        g.fillRect(0, 0, WIDTH, HEIGHT);

  
        g.dispose();
        bs.show();                    //para que muestre el buffer renderizado
    }

    private void tick() {
        handler.tick();

    }

    
    public static void main(String[] args){
    	Map map = new Map();
    }
}
